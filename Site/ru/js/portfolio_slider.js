$(function(){
$("#elastic_grid_demo").elastic_grid({	
	'hoverDirection': true,
	'hoverDelay': 100,
	'hoverInverse': false,
	'expandingSpeed': 500,
	'expandingHeight': 500,
	'items' :
		[
			{
			    'title': 'Ingage (Underwriting Process Manager)',
			    'description': ' Life Insurance web portal and application services that help to automate process of insurance, implementing whole life cycle of insurance business campaigns . My roles and responsibilities: application developer in team of 3 developers; providing support, configuring, enhancement of existing application, developing new web portal with number of form wizards, developing reporting web-site',
			    'thumbnail': ['img/portfolio/small/ingage3.jpg', 'img/portfolio/small/ingage2.jpg', 'img/portfolio/small/ingage1.jpg'],
			    'large': ['img/portfolio/large/ingage3.jpg', 'img/portfolio/large/ingage2.jpg', 'img/portfolio/large/ingage1.jpg'],
			'button_list'   :
			[
			{ 'title': 'Primary site', 'url': 'http://www.pega.com/insurance-underwriting-software', 'new_window': 'true' }
			],
			'tags': ['Web Apps', 'Web Sites', 'Web Services',   'All']
			},

			{
			'title': 'GA - Global Insurance Agency',
			'description'   : 'Distributed desktop application implementing whole life cycle of insurance business campaigns. My roles and responsibilities: application and database developer in team of 2-3 developers; providing support, configuring, enhancement of existing application, developing distributed desktop application with number of forms, developing database',
			'thumbnail': ['img/portfolio/small/ga1.jpg', 'img/portfolio/small/ga3.jpg', 'img/portfolio/small/ga4.jpg', 'img/portfolio/small/ga5.jpg'],
			'large': ['img/portfolio/large/ga1.jpg',  'img/portfolio/large/ga3.jpg', 'img/portfolio/large/ga4.jpg', 'img/portfolio/large/ga5.jpg'],
			'button_list'   :
			[
			{ 'title': 'Primary site', 'url': 'http://www.globalriskllc.com/business_insurance.php', 'new_window': 'true' }
			],
			'tags': ['All', 'Web Services',  'Distributed Systems']
			},

			{
			    'title': 'Acquire Web Real Time',
			    'description': 'This is marketing lead processing system and service API that includes web-based portal and web services and implements upload customer leads and manages whole life cycle of marketing campaigns based on .Net configuration manager framework. My roles and responsibilities: lead developer in team of 2-3 developers; designing architecture, developing; implementation includes number of services, SOA and REST API, load-balanced mechanism of services work',
			    'thumbnail': ['img/portfolio/small/aw1.jpg', 'img/portfolio/small/aw2.jpg', 'img/portfolio/small/aw3.jpg', 'img/portfolio/small/aw4.jpg', 'img/portfolio/small/aw5.jpg'],
			    'large': ['img/portfolio/large/aw1.jpg', 'img/portfolio/large/aw2.jpg', 'img/portfolio/large/aw3.jpg', 'img/portfolio/large/aw4.jpg', 'img/portfolio/large/aw5.jpg'],
			    'button_list':
			[
			{ 'title': 'Primary site', 'url': 'http://acquireweb.com/','new_window' : 'true' }
			],
			    'tags': ['All', 'Web Apps', 'Web Sites', 'Web Services', 'Distributed Systems']
			},

			{
			'title' : 'Ascend (Data Validator)',
			'description': 'Multilayer data processing system. .NET web-based portal and web services implementing upload customer leads and managing whole life cycle of marketing campaigns. Contains configuration portal and back-end services which are '+
			    'used to create data processing scenarios. The system includes' +
			    ' rich functional of multilayer data validation, customizable input and delivery part and reporting. My roles and responsibilities: senior developer in team of 2-3 developer; designing architecture, developing; implementation includes number of services, administration web-site',
			'thumbnail': ['img/portfolio/small/dv_1.jpg', 'img/portfolio/small/dv_2.jpg', 'img/portfolio/small/dv_3.jpg', 'img/portfolio/small/dv_4.jpg', 'img/portfolio/small/dv_5.jpg', 'img/portfolio/small/dv_6.jpg', 'img/portfolio/small/dv_7.jpg', 'img/portfolio/small/dv_8.jpg', 'img/portfolio/small/dv_9.jpg'],
			'large': ['img/portfolio/large/dv_1.jpg', 'img/portfolio/large/dv_2.jpg', 'img/portfolio/large/dv_3.jpg', 'img/portfolio/large/dv_4.jpg', 'img/portfolio/large/dv_5.jpg', 'img/portfolio/large/dv_6.jpg', 'img/portfolio/large/dv_7.jpg', 'img/portfolio/large/dv_8.jpg', 'img/portfolio/large/dv_9.jpg'],
			'button_list':
			[
			{ 'title': 'Demo site', 'url': 'http://data-validator.com:8080', 'new_window': 'true' }
		
			],
			'tags': ['All', 'Web Apps', 'Web Sites', 'Web Services', 'Distributed Systems']
			},

			{
			'title': 'AcquireLocal',
			'description': 'AcquireLocal is a multi-touch frequently-based program that gives customers the ability to select their prospects with pinpoint accuracy and then reach them several times with different creatives, while effectively merging online marketing with in-house mail delivery. My roles and responsibilities: database designing; web site and back-end part development; support and enhancement',
			'thumbnail': ['img/portfolio/small/aql_1.jpg', 'img/portfolio/small/aql_2.jpg', 'img/portfolio/small/aql_3.jpg'],
			'large': ['img/portfolio/large/aql_1.jpg', 'img/portfolio/large/aql_2.jpg', 'img/portfolio/large/aql_3.jpg'],
			'button_list':
			[
		
			],
			'tags': ['All', 'Web Apps', 'Web Sites', 'Web Services']
			},

			{
			    'title': 'Scenario Web Extractor',
			    'description': 'Scenario Web Extractor tool allows you to extract data in different formats from search engine results and different URL addresses using predefined scenarios. Using it you can extract data such as emails, proxies, phones, part of news or blog site content, any special data needed. My roles and responsibilities: product owner; whole tool developing',
			    'thumbnail': ['img/portfolio/small/swe_2.jpg', 'img/portfolio/small/swe_1.jpg', 'img/portfolio/small/swe_3.jpg', 'img/portfolio/small/swe_4.jpg', 'img/portfolio/small/swe_5.jpg'],
			    'large': ['img/portfolio/large/swe_2.jpg', 'img/portfolio/large/swe_1.jpg', 'img/portfolio/large/swe_3.jpg', 'img/portfolio/large/swe_4.jpg', 'img/portfolio/large/swe_5.jpg'],
			    'button_list':
                [
                    { 'title': 'Tool on codecayon', 'url': 'http://codecanyon.net/item/scenario-web-extractor/10906370', 'new_window': 'true' },
                { 'title': 'My codecayon profile', 'url': 'http://codecanyon.net/user/RoarBear', 'new_window': 'true' },
                { 'title': 'Demo video', 'url': 'https://youtu.be/9z1iZo7m1SQ', 'new_window': 'true' }

                ],
			    'tags': ['All',  'SEO Tools']
			},
             {
                 'title': 'Youtube Visitor',
                 'description': 'YouTube Visitor tool allows you to generate your YouTube clip views. The tool provide full browser emulation support using embedded browser. Parallels watchers and proxies supported. My roles and responsibilities: product owner; whole tool developing',
                 'thumbnail': ['img/portfolio/small/yv_1.jpg', 'img/portfolio/small/yv_2.jpg', 'img/portfolio/small/yv_3.jpg', 'img/portfolio/small/yv_4.jpg'],
                 'large': ['img/portfolio/large/yv_1.jpg', 'img/portfolio/large/yv_2.jpg', 'img/portfolio/large/yv_3.jpg', 'img/portfolio/large/yv_4.jpg'],
                 'button_list':
                 [
                     { 'title': 'Tool on codecayon', 'url': 'http://codecanyon.net/item/youtube-visitor/11224775', 'new_window': 'true' },
                 { 'title': 'My codecayon profile', 'url': 'http://codecanyon.net/user/RoarBear', 'new_window': 'true' },
                 { 'title': 'Demo video', 'url': 'https://youtu.be/x4fFceCoDp0', 'new_window': 'true' }

                 ],
                 'tags': ['All',  'SEO Tools']
             },
            {
                'title': 'Site Visitor',
                'description': 'Site Visitor tool allows you to generate traffic on your site(s). The tool provide full browser emulation so all scripts (including analytics such as google analysts) will execute on pages. You can configure input flow and see all processing in real-time. Search engine traffic generation supported (currently Google and Bing). My roles and responsibilities: product owner; whole tool developing',
                'thumbnail': ['img/portfolio/small/sv_4.jpg','img/portfolio/small/sv_1.jpg', 'img/portfolio/small/sv_2.jpg', 'img/portfolio/small/sv_3.jpg'],
                'large': ['img/portfolio/large/sv_4.jpg','img/portfolio/large/sv_1.jpg', 'img/portfolio/large/sv_2.jpg', 'img/portfolio/large/sv_3.jpg'],
                'button_list':
                [
                    { 'title': 'Tool on codecayon', 'url': 'http://codecanyon.net/item/site-visitor/11215443', 'new_window': 'true' },
                { 'title': 'My codecayon profile', 'url': 'http://codecanyon.net/user/RoarBear', 'new_window': 'true' },
                { 'title': 'Demo video', 'url': 'https://youtu.be/YVVfzUt96DU', 'new_window': 'true' }

                ],
                'tags': ['All',  'SEO Tools']
            },
            
            {
                'title': 'PET (Psycho Ergo Test System)',
                'description': 'Automated hardware-software system that designed for psychological testing. It includes a client-server application, database, a set of modules and test devices for psycho-physiological diagnostics. The system includes 40+ QA form tests, 20+ tests that include functionallity to use hardware devices - game wheel, joystick, biathlon gun emulator, the vibro exerciser, tracker glasses and other. The system includes extensive reporting and result interpretation functionality and ability to create test packages for different fields of activity. Currently the system has been installed in several enterprises in Belarus including staff departments, schools, driving schools, industrial enterprises, private psychological offices, some departments in sport organizations. My roles and responsibilities: lead developer in team of 4-5 developers; designing architecture, developing',
                'thumbnail': ['img/portfolio/small/pet_1.jpg', 'img/portfolio/small/pet_2.jpg', 'img/portfolio/small/pet_3.jpg', 'img/portfolio/small/pet_4.jpg', 'img/portfolio/small/pet_5.jpg', 'img/portfolio/small/pet_6.jpg',
                'img/portfolio/small/pet_7.jpg', 'img/portfolio/small/pet_8.jpg', 'img/portfolio/small/pet_9.jpg' ],
                'large': ['img/portfolio/large/pet_1.jpg', 'img/portfolio/large/pet_2.jpg', 'img/portfolio/large/pet_3.jpg', 'img/portfolio/large/pet_4.jpg', 'img/portfolio/large/pet_5.jpg', 'img/portfolio/large/pet_6.jpg', 'img/portfolio/large/pet_7.jpg', 'img/portfolio/large/pet_8.jpg', 'img/portfolio/large/pet_9.jpg'],
                'button_list':
                [
                  
                { 'title': 'Dealer Site', 'url': 'http://www.rchp.bsu.by/Testirovanie/Psychoergotest', 'new_window': 'true' }

                ],
                'tags': ['All',   'Automated Workstations']
            },

             {
                 'title': 'FSA (Forecasting Sporting Achievements System)',
                 'description': 'Automated forecasting sporting workstation. The system is based on biochemical and physiological sportsman indicators and sportsman competition results and provides numerous intelligent mechanizms, methods and algorithms that using for forecasting sporting achievements in biatlon, ski races. My roles and responsibilities: lead developer in team of 2 developers; designing architecture, developing, analysing data, projecting database',
                 'thumbnail': ['img/portfolio/small/fsa_1.jpg', 'img/portfolio/small/fsa_2.jpg', 'img/portfolio/small/fsa_3.jpg', 'img/portfolio/small/fsa_4.jpg', 'img/portfolio/small/fsa_5.jpg'],
                 'large': ['img/portfolio/large/fsa_1.jpg', 'img/portfolio/large/fsa_2.jpg', 'img/portfolio/large/fsa_3.jpg', 'img/portfolio/large/fsa_4.jpg', 'img/portfolio/large/fsa_5.jpg'],
                 'button_list':
                 [

                 { 'title': 'Presentation', 'url': 'doc/fsa.pptx', 'new_window': 'true' }

                 ],
                 'tags': ['All',   'Automated Workstations']
             },

             {
                 'title': 'Novatour',
                 'description': 'Novatour shop site. My roles and responsibilities: developing and supporting site',
                 'thumbnail': ['img/portfolio/small/novatour_1.jpg', 'img/portfolio/small/novatour_2.jpg', 'img/portfolio/small/novatour_3.jpg'],
                 'large': ['img/portfolio/large/novatour_1.jpg', 'img/portfolio/large/novatour_2.jpg', 'img/portfolio/large/novatour_3.jpg'],
                 'button_list':
                 [

                 { 'title': 'Site URL', 'url': 'http://www.novatour.ru/', 'new_window': 'true' }

                 ],
                 'tags': ['All', 'Web Sites']
             },
             {
                 'title': 'Roar Bear Soft Site',
                 'description': 'Responsive web site. My roles and responsibilities: site owner; developing site',
                 'thumbnail': ['img/portfolio/small/roar_1.jpg', 'img/portfolio/small/roar_2.jpg', 'img/portfolio/small/roar_3.jpg', 'img/portfolio/small/roar_4.jpg', 'img/portfolio/small/roar_5.jpg'],
                 'large': ['img/portfolio/large/roar_1.jpg', 'img/portfolio/large/roar_2.jpg', 'img/portfolio/large/roar_3.jpg', 'img/portfolio/large/roar_4.jpg', 'img/portfolio/large/roar_5.jpg'],
                 'button_list':
                 [

                 { 'title': 'Site URL', 'url': 'http://www.roarbear.net/', 'new_window': 'true' }

                 ],
                 'tags': ['All', 'Web Sites']
             },
             {
                 'title': 'RadioStart - Internet radio station',
                 'description': 'Internet radio station. Web site and mobile, desktop clients that allow user to chat, talk with speakers and read news. My roles and responsibilities: application developer; developing windows and mobile clients',
                 'thumbnail': ['img/portfolio/small/rs_1.jpg', 'img/portfolio/small/rs_2.jpg'],
                 'large': ['img/portfolio/large/rs_1.jpg', 'img/portfolio/large/rs_2.jpg'],
                 'button_list':
                 [

                 { 'title': 'Site URL', 'url': 'http://www.radiostart.by/', 'new_window': 'true' }

                 ],
                 'tags': ['All', 'Web Sites']
             }
		]
	});
});